package com.example.demo.producer;

import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Component
@RestController
public class ProducerService {

	public String queue="priority-queue";
	
	@Autowired
	Session session;
	@Autowired
	MessageProducer messageProducer;
	
	@PostMapping("message/{message}/{priority}")
    public String send(@PathVariable("message")String message,@PathVariable("priority")int priority) throws JMSException{
	    TextMessage textMessage = session.createTextMessage(message);
	    messageProducer.send(textMessage, DeliveryMode.PERSISTENT, priority, 0);
	    return "OK";
    }
}
