package com.example.demo.consumer;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.TextMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Component
@RestController
@RequestMapping("/api")
public class ConsumerService {
	@Autowired
	MessageConsumer messageConsumer;
	@GetMapping("message/consume")
	public String receive() throws JMSException {
		Message message = messageConsumer.receive();
	    TextMessage input = (TextMessage) message;
	    String text = input.getText();
	    return text;
	}
}
